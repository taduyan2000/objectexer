<?php 
    class Book {
        public $name;
        public $isbn;
        public $author;
        public $price;

        public function __construct($name, $isbn, $author, $price) {
            $this->name = $name;
            $this->isbn = $isbn;
            $this->author = $author;
            $this->price = $price;
        }

        function get_name() {
            return $this->name;
        }
        
        function get_isbn() {
            return $this->isbn;
        }

        function get_author() {
            return $this->author;
        }

        function get_price() {
            return $this->price;
        }

        function printinfo() {
            echo "Tên sách: {$this->name} <br>";
            echo "Mã sách: {$this->isbn} <br>";
            echo "Tên tác giả: {$this->author} <br>";
            echo "Giá sách: {$this->price} <br>";
        }
    }

    class Textbook extends Book {

    }

    class Novel extends Book {
        public $old;
        
        public function __construct($name, $isbn, $author, $price, $old) {
            $this->name = $name;
            $this->isbn = $isbn;
            $this->author = $author;
            $this->price = $price;
            $this->old = $old;
        }

        function get_old() {
            return $this->old;
        }

        function printinfo() {
            echo "Tên sách: {$this->name} <br>";
            echo "Mã sách: {$this->isbn} <br>";
            echo "Tên tác giả: {$this->author} <br>";
            echo "Giá sách: {$this->price} <br>";
            echo "Độ tuổi phù hợp: {$this->old} <br>";
        }
    }

    $daiso = new Textbook("Đại Số", "T100", "Nguyễn Văn A", "19000đ");
    $nhagiakim = new Novel("Nhà Giả Kim","NV22", "Paulo Coelho", "30000đ", "16+");
?>