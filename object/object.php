<?php 
    class Car {
        public $name;
        public $model;
        public $color;
        public $price;
    
        // __construct()
        public function __construct($name, $model, $color, $price) {
            $this->name = $name;
            $this->model = $model;
            $this->price = $price;
            $this->color = $color; 
        }

        function get_name() {
            return $this->name;
        }

        function get_model() {
            return $this->model;
        }

        function get_color() {
            return $this->color;
        }

        function get_price() {
            return $this->price;
        }

        function printinfo() {
            echo "Name:  {$this->name}  <br>";
            echo "Model: {$this->model} <br>";
            echo "Color: {$this->color} <br>";
            echo "Price: {$this->cprice} <br>";
        }
    }
        $kiamorning = new Car("Kiamorning","M500", "White", "50000$");

        $ford = new Car("Ford","G700","Gray","90000$");
?>